#!/usr/bin/env bash

rm -f ./output/*.png ./output/*.html ./output/*.xml
source .env

# pybot --timestampoutput --outputdir ./output --argumentfile ./args/chrome-args.txt \
#   ./tests/*.robot

pybot --timestampoutput --outputdir ./output --argumentfile ./args/chrome-args.txt \
  --test "User attempts to fetch next lead" ./tests/*-tests.robot
