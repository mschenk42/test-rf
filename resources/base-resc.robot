# -*- coding: robot -*-

*** Settings ***

Documentation
Library       OperatingSystem
Library       String
Library       Selenium2Library  run_on_failure=Save Screenshot  timeout=${SELENIUM_TIMEOUT}  implicit_wait=${SELENIUM_IMPLICIT_WAIT}

*** Keywords ***

Save Screenshot
  ${timestamp}=  Get Time
  ${filename}=  Replace String Using Regexp  ${timestamp}  \\W  ${EMPTY}
  Capture Page Screenshot  ${filename}.png

Start Charles Proxy
  Go To  http://control.charles
  Click Link  recording/
  Click Link  start
  Go To  http://control.charles
  Click Link  session/
  Click Link  clear

End Charles Proxy
  Go To  http://control.charles
  Click Link  recording/
  Click Link  stop
  Go To  http://control.charles
  Click Link  session/
  Run Keyword If Test Failed  Click Link  download
  Sleep  2s

Setup
  [Arguments]  ${browser}=${BROWSER}
  Run Keyword If  '${browser}' == 'phantomjs'  Setup PhantomJS
  Run Keyword Unless  '${browser}' == 'phantomjs'  Create Browser
  Maximize Browser Window
  Run Keyword If  '${PROXY}' == 'true'  Start Charles Proxy

Teardown
  Run Keyword If  '${PROXY}' == 'true'  End Charles Proxy
  Close Browser

Screenshot
  Run Keyword If  '${SCREENSHOT}' == 'true'  Run Keywords  Sleep  .5s  AND  Save Screenshot

Create Browser
  ${DCAPS}=  Replace Variables  ${DCAPS}
  Open Browser  ${START_PAGE}  browser=${browser}  ff_profile_dir=${FF_PROFILE}  remote_url=${REMOTE_URL}  desired_capabilities=${DCAPS}

Setup PhantomJS
  ${service args}=  Create List  --ignore-ssl-errors=true  --ssl-protocol=any
  Create Webdriver  PhantomJS  service_args=${service args}

Click Visible Link
  [Arguments]  ${selector}
  Wait Until Keyword Succeeds  10  1  Wait Until Element Is Visible  ${selector}  10
  Click Link  ${selector}

Click Visible Button
  [Arguments]  ${selector}
  Wait Until Keyword Succeeds  10  1  Wait Until Element Is Visible  ${selector}  10
  Click Button  ${selector}

Generate Unique Name
  [Arguments]  ${prefix}=''  ${no_of_digits}=6
  ${rand_num}=  Generate Random String  ${no_of_digits}  [NUMBERS]
  [return]  ${prefix}${rand_num}
