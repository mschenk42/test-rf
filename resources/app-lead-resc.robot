# -*- coding: robot -*-

*** Settings ***
Library    ImapLibrary
Resource  ../resources/base-resc.robot

*** Keywords ***

Fetch Next Lead
  Wait Until Page Contains  Fetch Next Lead
  Click Link  /leads/fetch
