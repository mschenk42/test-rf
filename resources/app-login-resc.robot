# -*- coding: robot -*-

*** Settings ***
Library    ImapLibrary
Resource  ../resources/base-resc.robot

*** Keywords ***

Goto App Login
  Go To  ${HOST_APP}
  Wait Until Page Contains  Sign in to start your session

Input Phone Number "${phone}"
  Wait Until Page Contains Element  css=#phone
  Input Text  css=#phone  ${phone}
  Click Button  css=.btn.btn-primary.btn-block.btn-flat.js-get-pin

Input Pin "${email}" "${password}" "${subject}"
  Open Mailbox  host=imap.googlemail.com  user=${email}  password=${password}
  ${INDEX} =  Wait For Email  subject=${subject}  timeout=60  poll_frequency=5
  ${BODY} =  Get Email Body  ${INDEX}
  ${LINE} =  Get Line  ${BODY}  0
  ${PIN} =  Replace String  ${LINE}  Your access PIN is  ${EMPTY}
  ${PIN} =  Should Match Regexp  ${PIN}  \\d{4}
  Delete Email  ${INDEX}
  Close Mailbox

  Wait Until Page Contains Element  css=#pin
  Input Text  css=#pin  ${PIN}
  Click Button  css=.btn.btn-success.btn-block.btn-flat.js-login

Logout App
  Go To  ${HOST_APP}/logout
