# -*- coding: robot -*-

*** Settings ***
Library    String
Resource  ../resources/app-login-resc.robot
Resource  ../resources/app-lead-resc.robot

Suite Setup  Setup
Suite Teardown  Teardown


*** Test Cases ***


User attempts to fetch next lead
  Goto App Login

  Input Phone Number "4143011642"
  Wait Until Page Contains  Sign In

  Input Pin "%{TEST83B_EMAIL}" "%{TEST83B_PASSWORD}" "Your PIN"
  Wait Until Page Contains  Fetch Next Lead

  Fetch Next Lead
  Wait Until Page Contains  Logged in user has no current shift
