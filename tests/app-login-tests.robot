# -*- coding: robot -*-

*** Settings ***
Library    String
Resource  ../resources/app-login-resc.robot

Suite Setup  Setup
Suite Teardown  Teardown


*** Test Cases ***

User recieves pin via email and logs into app
  GoTo App Login

  Input Phone Number "4143011642"
  Wait Until Page Contains  Sign In

  Input Pin "%{TEST83B_EMAIL}" "%{TEST83B_PASSWORD}" "Your PIN"
  Wait Until Page Contains  Fetch Next Lead

  Logout App


User attempts to login with invalid phone number
  GoTo App Login

  Input Phone Number "123"
  Alert Should Be Present  text=Please provide a valid US phone number to receive your login pin.

  Input Phone Number "1234567890"
  Alert Should Be Present  text=Please provide a valid US phone number to receive your login pin.
